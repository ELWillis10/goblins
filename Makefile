all: compile docs check

compile:
	raco make -l goblins

test: check

check:
	raco test goblins

clean:
	find goblins -name compiled | xargs rm -rf
	rm -rf goblins/doc

doc: docs

docs:
	scribble --dest goblins/doc +m --htmls goblins/scribblings/goblins.scrbl
