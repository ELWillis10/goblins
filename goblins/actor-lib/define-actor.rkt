#lang racket

;;; Copyright 2021 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;; WARNING: this syntax has not really "settled" yet.
;;   We are debating whether or not to use positional arguments
;;   or keyword arguments for the macro.  Yes, keywords are probably
;;   better...

(provide define-actor)

(require "../core.rkt"
         (for-syntax syntax/parse))

(define-syntax (define-actor stx)
  (syntax-parse stx
    [(define-actor (actor-id args ...) [bcom self] body ...)
     #'(define (actor-id args ...)
         ;; internal constructor
         (define (cstr bcom)
           body ...)
         (define self (spawn-named 'actor-id cstr))
         self)]
    [(define-actor (actor-id args ...) [bcom] body ...)
     #'(define-actor (actor-id args ...) [bcom _] body ...)]
    [(define-actor (actor-id args ...) [] body ...)
     #'(define-actor (actor-id args ...) [_ _] body ...)]))

(module+ test
  (require rackunit
           "methods.rkt"
           "bootstrap.rkt")

  (define am (make-actormap))
  (define-actormap-run am-run am)

  (define-actor (^cell [initial-val #f])
    [bcom]
    (let beh ([val initial-val])
      (methods
       [(get) val]
       [(set new-val) (bcom (beh new-val))])))

  (define my-cell (am-run (^cell)))
  (check-equal? (am-run ($ my-cell 'get)) #f)
  (am-run ($ my-cell 'set 'gold))
  (check-equal? (am-run ($ my-cell 'get)) 'gold)
  
  (define-actor (^knows-self my-name)
    [bcom self]
    (methods
     [(eq-me? obj) (eq? obj self)]
     [(equal-name? this-name)
      (equal? this-name my-name)]))

  (define selfy1 (am-run (^knows-self "selfy")))
  (define selfy2 (am-run (^knows-self "selfy")))

  (check-true (am-run ($ selfy1 'eq-me? selfy1)))
  (check-false (am-run ($ selfy1 'eq-me? selfy2))))
