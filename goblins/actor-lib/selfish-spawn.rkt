#lang racket/base

;;; Copyright 2020 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(provide selfish-spawn)

(require "../core.rkt")

(define (^selfish-start bcom constructor kws kw-vals args)
  (lambda (self)
    (bcom (keyword-apply constructor kws kw-vals bcom self args))))

(define selfish-spawn
  (make-keyword-procedure
   (lambda (kws kw-vals constructor . args)
     (define renamed-selfish-start
       (procedure-rename ^selfish-start (object-name constructor)))
     (define self
       (spawn renamed-selfish-start constructor kws kw-vals args))
     ;; now transition to the version with self
     ($ self self)
     self)))

(module+ test
  (require rackunit)
  (define am (make-actormap))
  (define (^narcissus bcom self stare-object)
    (lambda (how-i-feel)
      `(i-am ,self i-stare-into ,stare-object and-i-feel ,how-i-feel)))
  (define narcissus
    (actormap-run!
     am (lambda ()
          (selfish-spawn ^narcissus 'water))))
  (test-equal?
   "selfish-spawned actors know themselves"
   (actormap-peek am narcissus 'transfixed)
   `(i-am ,narcissus i-stare-into water and-i-feel transfixed)))
